/*
 * Copyright (C) 2012 Square, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.squareup.otto.modified;

import java.util.Map;
import java.util.Set;

/** Finds producer and subscriber methods. */
interface HandlerFinder {

  Map<Class<?>, EventProducer> findAllProducers(Object listener);

  Map<Class<?>, Set<com.squareup.otto.modified.EventHandler>> findAllSubscribers(Object listener);

  boolean quickCheck(Class<?> clazz);

  HandlerFinder ANNOTATED = new HandlerFinder() {
    @Override
    public Map<Class<?>, EventProducer> findAllProducers(Object listener) {
      return com.squareup.otto.modified.AnnotatedHandlerFinder.findAllProducers(listener);
    }

    @Override
    public Map<Class<?>, Set<com.squareup.otto.modified.EventHandler>> findAllSubscribers(Object listener) {
      return com.squareup.otto.modified.AnnotatedHandlerFinder.findAllSubscribers(listener);
    }

    @Override
    public boolean quickCheck(Class<?> clazz) {
      return com.squareup.otto.modified.AnnotatedHandlerFinder.quickCheck(clazz);
    }

  };
}
