package me.josephzhang.common;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;

import me.josephzhang.common.constant.DialogId;
import me.josephzhang.common.util.Logger;

public class BaseActivity extends AppCompatActivity {

  protected final Logger logger = Logger.with(getClass());
  private boolean hasKeyboardListener = false;
  private boolean keyboardVisible = false;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }


  protected void initActionBar(Toolbar toolbar) {
    setSupportActionBar(toolbar);
    // More setups here
  }

  /**
   * Hook for Activities that care about keyboard visibility. MUST CALL
   * registerKeyboardStateListener() FIRST!
   *
   * @param visible
   */
  protected void onKeyBoardStateChanged(boolean visible) {

  }

  protected void registerKeyboardStateListener() {
    hasKeyboardListener = true;
    View root = this.findViewById(android.R.id.content);
    if (root != null) {
      registerKeyboardListenerImpl(root);
    }
  }

  @Override
  protected void onResume() {
    super.onResume();
  }

  @Override
  protected void onPause() {
    super.onPause();
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == android.R.id.home) {
      finish();
      return true;
    } else {
      return super.onOptionsItemSelected(item);
    }
  }

  @Override
  public void finish() {
    super.finish();
  }

  protected DialogFragment onCreateDialogFragment(int id) {
    switch (id) {
      case DialogId.LOADING:
        return new LoadingDialogFragment();
      default:
        return null;
    }
  }

  public final void showDialogFragment(int id) {
    if (isFinishing()) {
      return;
    }
    DialogFragment f = onCreateDialogFragment(id);
    if (f != null) {
      FragmentTransaction ft = removeDialogInner(id);
      if (ft == null) {
        ft = getSupportFragmentManager().beginTransaction();
      }
      f.show(ft, "dialog-id:" + id);
    }
  }

  public final void removeDialogFragment(int id) {
    if (isFinishing()) {
      return;
    }
    FragmentTransaction ft = removeDialogInner(id);
    if (ft != null) {
      ft.commitAllowingStateLoss();
    }
  }

  private FragmentTransaction removeDialogInner(int id) {
    Fragment former = getSupportFragmentManager().findFragmentByTag(
      "dialog-id:" + id);
    if (former != null) {
      FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
      ft.remove(former);
      return ft;
    }
    return null;
  }

  private void registerKeyboardListenerImpl(final View root) {
    if (root == null) {
      logger.e("root == null!");
      return;
    }
    root.getViewTreeObserver().addOnGlobalLayoutListener(
      new OnGlobalLayoutListener() {
        @Override
        public void onGlobalLayout() {
          int heightDiff = root.getRootView().getHeight() - root.getHeight();
          if (heightDiff > 100) { // if more than 100 pixels, its probably a
            // keyboard...
            if (!keyboardVisible) {
              logger
                .d("has key board--> if this log occurs multiple times, check for dead loop!");
              onKeyBoardStateChanged(keyboardVisible = true);
            }
          } else {
            if (keyboardVisible) {
              logger
                .d("no key board!--> if this log occurs multiple times, check for dead loop!");
              onKeyBoardStateChanged(keyboardVisible = false);
            }
          }
        }
      });
  }
}
