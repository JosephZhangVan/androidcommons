package me.josephzhang.common.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.widget.BaseAdapter;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import me.josephzhang.common.util.Logger;

public abstract class BaseListAdapter<T> extends BaseAdapter {
  protected final List<T> list = new ArrayList<T>();
  protected Logger logger = Logger.with(getClass());
  protected Context context;
  protected Fragment fragment;
  protected LayoutInflater inflater;
  protected Picasso picasso;

  public BaseListAdapter(Fragment fragment) {
    this(fragment.getActivity());
    this.context = fragment.getActivity();
    this.fragment = fragment;
  }

  public BaseListAdapter(Context context) {
    this.context = context;
    this.inflater = LayoutInflater.from(context);
    this.picasso = Picasso.with(context);
  }

  @Override
  public int getCount() {
    return (this.list == null) ? 0 : this.list.size();
  }

  @Override
  public T getItem(int position) {
    return this.list.get(position);
  }

  @Override
  public long getItemId(int position) {
    return position;
  }

  @Override
  public boolean hasStableIds() {
    return true;
  }

  @Override
  public boolean isEmpty() {
    return (this.getCount() == 0);
  }

  /**
   * @param _list
   * @return
   * @see List#addAll(Collection)
   */
  public boolean addAll(Collection<? extends T> _list) {
    return this.list.addAll(_list);
  }

  public boolean add(T object) {
    return list.add(object);
  }

  /**
   * @see List#clear()
   */
  public void clear() {
    this.list.clear();
  }

  /**
   * @param location
   * @return
   * @see List#remove(int)
   */
  public T remove(int location) {
    return list.remove(location);
  }
}
