package me.josephzhang.common;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;

import me.josephzhang.common.constant.DialogId;

/**
 * Created by joseph on 15-11-01.
 */
public class LoadingDialogFragment extends BaseDialogFragment {

  @NonNull
  @Override
  public Dialog onCreateDialog(Bundle savedInstanceState) {
    ProgressDialog dialog = new ProgressDialog(getActivity());
    dialog.setMessage(getString(R.string.loading));
    return dialog;
  }

  @Override
  public int getDialogId() {
    return DialogId.LOADING;
  }
}
