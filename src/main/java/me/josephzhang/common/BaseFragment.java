package me.josephzhang.common;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.MenuItem;

import me.josephzhang.common.util.Logger;

public abstract class BaseFragment extends Fragment {
  protected Logger logger = Logger.with(getClass());

  public BaseFragment() {
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public void onActivityCreated(Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
  }

  @Override
  public void onResume() {
    super.onResume();
  }

  @Override
  public void onPause() {
    super.onPause();
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
  }

  @Override
  public void onDetach() {
    super.onDetach();

  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        Activity a = getValidActivity();
        if (a != null) {
          a.onBackPressed();
          return true;
        }
        break;
      default:
        break;
    }
    return super.onOptionsItemSelected(item);
  }

  @SuppressWarnings("deprecation")
  public void showDialog(int dialogId) {
    FragmentActivity a = getValidActivity();
    if (a instanceof BaseActivity) {
      ((BaseActivity) a).showDialogFragment(dialogId);
    } else if (a != null) {
      a.showDialog(dialogId);
    }
  }

  @SuppressWarnings("deprecation")
  public void removeDialog(int dialogId) {
    Activity a = getValidActivity();
    if (a instanceof BaseActivity) {
      ((BaseActivity) a).removeDialogFragment(dialogId);
    } else if (a != null) {
      a.removeDialog(dialogId);
    }
  }

  protected FragmentActivity getValidActivity() {
    FragmentActivity a = getActivity();
    if (a != null && !a.isFinishing() && isAdded()) {
      return a;
    } else {
      return null;
    }
  }

}
