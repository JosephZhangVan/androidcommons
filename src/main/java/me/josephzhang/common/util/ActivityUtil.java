package me.josephzhang.common.util;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import java.io.File;
import java.util.List;

public class ActivityUtil {

  private static Logger logger = Logger.with(ActivityUtil.class);
  private static int versionCode = 0;
  private static String versionNumber;
  private static String IMEI;
  private static String MAC;

  private static String getMac(Context context) {
    if (TextUtils.isEmpty(MAC)) {
      try {
        WifiManager localWifiManager = (WifiManager) context
                .getSystemService(Context.WIFI_SERVICE);
        WifiInfo localWifiInfo = localWifiManager.getConnectionInfo();
        MAC = localWifiInfo.getMacAddress();
      } catch (Exception e) {
        logger.e(e);
      }
    }
    return MAC;
  }

  private static String getIMEI(Context context) {
    if (TextUtils.isEmpty(IMEI)) {
      try {
        TelephonyManager manager = (TelephonyManager) context
                .getSystemService(Context.TELEPHONY_SERVICE);
        IMEI = manager.getDeviceId();
      } catch (Exception e) {
      }
    }
    return IMEI;
  }

  private static String getAndroidId(Context context) {
    String id = Settings.Secure.getString(context.getContentResolver(),
            "android_id");
    logger.d("getDeviceId: Secure.ANDROID_ID: " + id);
    return id;
  }

  public static File getTempFile(String file) {
    if (isSDCARDMounted()) {
      String destDirPath = Environment.getExternalStorageDirectory()
              + "/temp";
      File destDir = new File(destDirPath);
      if (!destDir.exists()) {
        destDir.mkdirs();
      }
      File f = new File(destDir, file);
      return f;
    } else {
      return null;
    }
  }

  public static boolean isSDCARDMounted() {
    return Environment.MEDIA_MOUNTED.equals(Environment
            .getExternalStorageState());
  }

  public static void setBoardcastReceiverEnabled(Context context,
                                                 Class<? extends BroadcastReceiver> clazz, boolean enabled) {
    PackageManager manager = context.getPackageManager();
    ComponentName name = new ComponentName(context, clazz);
    manager.setComponentEnabledSetting(name,
            enabled ? PackageManager.COMPONENT_ENABLED_STATE_ENABLED
                    : PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
            PackageManager.DONT_KILL_APP);
  }

  public static int getVersionCode(Context context) {
    if (versionCode == 0) {
      initVersionInfo(context);
    }
    return versionCode;
  }

  public static String getVersionName(Context context) {
    if (TextUtils.isEmpty(versionNumber)) {
      initVersionInfo(context);
    }
    return versionNumber;
  }

  private static void initVersionInfo(Context context) {
    PackageManager packageManager = context.getPackageManager();
    String packageName = context.getPackageName();
    try {
      PackageInfo info = packageManager.getPackageInfo(packageName, 0);
      versionCode = info.versionCode;
      versionNumber = info.versionName;
    } catch (NameNotFoundException e) {
      versionCode = 0;
      versionNumber = "Unknown";
    }
  }

  public static String getMetaDataString(Context context, String name) {
    ApplicationInfo info;
    String data;
    try {
      info = context.getPackageManager().getApplicationInfo(
              context.getPackageName(), PackageManager.GET_META_DATA);
      data = info.metaData.getString(name);
    } catch (NameNotFoundException e) {
      data = null;
    }
    return data;
  }

  public static boolean getMetaDataBoolean(Context context, String name) {
    return getMetaDataBoolean(context, name, false);
  }

  public static boolean getMetaDataBoolean(Context context, String name,
                                           boolean def) {
    ApplicationInfo info;
    boolean data;
    try {
      info = context.getPackageManager().getApplicationInfo(
              context.getPackageName(), PackageManager.GET_META_DATA);
      data = info.metaData.getBoolean(name, def);
    } catch (NameNotFoundException e) {
      data = def;
    }
    return data;
  }

  /**
   * check the task is already running or not.
   *
   * @param task
   * @return a task is already running or not
   */
  public static boolean checkTask(AsyncTask<?, ?, ?> task) {
    if (task != null && (task.getStatus() != AsyncTask.Status.FINISHED)) {
      return true;
    }
    return false;
  }

  /**
   * check the thread is already running or not.
   *
   * @param thread
   * @return a thread is already running or not
   */
  public static boolean checkThread(Thread thread) {
    return thread != null && thread.isAlive() && !thread.isInterrupted();
  }

  public static boolean checkIntent(Context context, Intent intent) {
    PackageManager manager = context.getPackageManager();
    List<ResolveInfo> infos = manager.queryIntentActivities(intent, 0);
    return infos != null && infos.size() > 0;
  }

}
