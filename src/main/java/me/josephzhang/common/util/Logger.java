package me.josephzhang.common.util;

import android.content.Context;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;

public class Logger {
  private static final Map<String, Logger> INSTANCES = new HashMap<String, Logger>();
  private static boolean enable = true;
  private static final boolean METHOD_TRACE = true;
  private static final int INDEX_INIT = -1;
  private static final int INDEX_ERROR = -2;
  private volatile static int index = INDEX_INIT;

  private String tag;

  private Logger(String tag) {
    this.tag = tag;
  }

  public static boolean init(Context context) {
    return enable = ActivityUtil.getMetaDataBoolean(context, "debugger", false);
  }

  /**
   * Get the logging method name from stack trace.
   * This method may be slow but it only runs in debug mode.
   *
   * @param log
   * @return
   */
  private static String withMethodName(String log) {
    if (METHOD_TRACE && enable) {
      final StackTraceElement[] ste = Thread.currentThread().getStackTrace();
      return getLog(ste, log);
    } else {
      return log;
    }
  }

  /**
   * @param ste
   * @param log
   * @return
   */
  private static String getLog(StackTraceElement[] ste, String log) {
    if (index == INDEX_ERROR) {
      return log;
    } else if (index == INDEX_INIT) {
      synchronized (Logger.class) {
        if (index == INDEX_INIT) {
          int i = 0;
          while (i < ste.length) {
            String className = ste[i].getClassName();
            String methodName = ste[i].getMethodName();
            // System.out.println("className:" + className + " methodName:"
            // + methodName);
            // System.out.println(Logger.class.getName());
            if (Logger.class.getName().equals(className)
                    && "withMethodName".equals(methodName)) {
              break;
            }
            i++;
          }
          i += 2;
          if (i < ste.length) {
            index = i;
          } else {
            index = INDEX_ERROR;
          }
        }
      }
      return getLog(ste, log);
    } else {
      if (index < ste.length) {
        StringBuilder sb = new StringBuilder(ste[index].getMethodName());
        sb.append("->");
        sb.append(log);
        return sb.toString();
      } else {
        return log;
      }
    }
  }

  /**
   * Initialize the logger for the given class.
   *
   * @param clazz
   * @return
   */
  public static synchronized Logger with(Class<?> clazz) {
    String _tag = clazz.getName();
    Logger _logger = null;
    if (INSTANCES.containsKey(_tag)) {
      _logger = INSTANCES.get(_tag);
    } else {
      _logger = new Logger(_tag);
      INSTANCES.put(_tag, _logger);
    }

    return _logger;
  }

  public void v(String message) {
    if (enable) {
      Log.v(this.tag, withMethodName(message));
    }
  }

  public void v(String message, Throwable t) {
    if (enable) {
      Log.v(this.tag, withMethodName(message), t);
    }
  }

  public void d(String message) {
    if (enable) {
      Log.d(this.tag, withMethodName(message));
    }
  }

  public void d(String message, Throwable t) {
    if (enable) {
      Log.d(this.tag, withMethodName(message), t);
    }
  }

  public void i(String message) {
    if (enable) {
      Log.i(this.tag, withMethodName(message));
    }
  }

  public void i(String message, Throwable t) {
    if (enable) {
      Log.i(this.tag, withMethodName(message), t);
    }
  }

  public void w(String message) {
    if (enable) {
      Log.w(this.tag, withMethodName(message));
    }
  }

  public void w(Throwable t) {
    if (enable) {
      Log.w(this.tag, t.getMessage(), t);
    }
  }

  public void w(String message, Throwable t) {
    if (enable) {
      Log.w(this.tag, withMethodName(message), t);
    }
  }

  public void e(String message) {
    Log.e(this.tag, withMethodName(message));
  }

  public void e(Throwable t) {
    Log.e(this.tag, t.getMessage(), t);
  }

  public void e(String message, Throwable t) {
    Log.e(this.tag, withMethodName(message), t);
  }
}
