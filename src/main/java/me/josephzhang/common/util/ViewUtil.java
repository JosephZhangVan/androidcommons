package me.josephzhang.common.util;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ViewUtil {

  private static Logger logger = Logger.with(ViewUtil.class);
  private static final int[] STATE_PRESSED = new int[]{android.R.attr.state_pressed};
  private static final int[] STATE_FOCUSED = new int[]{android.R.attr.state_focused};
  private static final int[] STATE_NORMAL = new int[]{};

  private static int screenWidth;
  private static int screenHeight;
  private static float density;

  private ViewUtil() {
  }

  public static boolean resizeAcceptableBitmapToFile(File source, File dist) {
    if (source == null) {
      logger.e("file is empty!");
      return false;
    }
    OutputStream os = null;
    try {
      Bitmap bitmap = ViewUtil.getAcceptableBitmapFromFile(1080, source.getAbsolutePath());
      os = new FileOutputStream(dist);
      os = new BufferedOutputStream(os);
      bitmap.compress(Bitmap.CompressFormat.JPEG, 50, os);
      return true;
    } catch (FileNotFoundException e) {
      logger.e(e);
      return false;
    } finally {
      if (os != null) {
        try {
          os.close();
        } catch (IOException e) {
          logger.e(e);
        }
      }
    }
  }

  public static boolean resizeAcceptableBitmapToFile(Context context, Uri source, File dist) {
    if (source == null) {
      logger.e("uri is empty!");
      return false;
    }
    OutputStream os = null;
    try {
      Bitmap bitmap = ViewUtil.getAcceptableBitmapFromUri(context, 1080, source);
      os = new FileOutputStream(dist);
      os = new BufferedOutputStream(os);
      bitmap.compress(Bitmap.CompressFormat.JPEG, 50, os);
      return true;
    } catch (Exception e) {
      logger.e(e);
      return false;
    } finally {
      if (os != null) {
        try {
          os.close();
        } catch (IOException e) {
          logger.e(e);
        }
      }
    }
  }

  public static View findViewInListView(ListView listView, int position) {
    int count = listView.getCount();
    for (int i = 0; i < count; i++) {
      View view = listView.getChildAt(i);
      int pos = listView.getPositionForView(view);
      if (pos == position) {
        return view;
      }
    }
    return null;
  }

  public static float getScreenDensity(Context context) {
    if (density > 0) {
      return density;
    } else {
      initWindowProp(context);
      return density;
    }
  }

  public static int getScreenWidth(Context context) {
    if (screenWidth > 0) {
      return screenWidth;
    } else {
      initWindowProp(context);
      return screenWidth;
    }
  }

  public static int getScreenHeight(Context context) {
    if (screenHeight > 0) {
      return screenHeight;
    } else {
      initWindowProp(context);
      return screenHeight;
    }
  }

  public static Drawable generateStateListDrawable(Drawable normal,
                                                   Drawable pressed) {
    StateListDrawable drawable = new StateListDrawable();
    drawable.addState(STATE_PRESSED, pressed);
    drawable.addState(STATE_FOCUSED, pressed);
    drawable.addState(STATE_NORMAL, normal);
    return drawable;
  }

  public static Drawable drawTextInside9Patch(String text, int textColor,
                                              float textSize, Drawable drawable) {
    Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    paint.setTextSize(textSize);
    paint.setColor(textColor);
    paint.setTextAlign(Align.CENTER);
    int textHeight = (int) (paint.descent() - paint.ascent() + 0.5f);
    int textWidth = (int) (paint.measureText(text, 0, text.length()) + 0.5f);
    logger.d("textWidth:" + textWidth);
    Rect padding = new Rect();
    drawable.getPadding(padding);
    logger.d("paddings:" + padding.toShortString());
    Rect bounds = new Rect(0, 0, padding.left + padding.right + textWidth,
      padding.top + padding.bottom + textHeight);
    Bitmap bitmap = Bitmap.createBitmap(bounds.width(), bounds.height(),
      Config.ARGB_8888);
    Canvas canvas = new Canvas(bitmap);
    drawable.setBounds(bounds);
    drawable.draw(canvas);
    canvas.drawText(text, padding.left + textWidth / 2f, padding.top
      + textHeight - paint.descent(), paint);
    drawable.getPadding(padding);
    logger.d("paddings:" + padding.toShortString());
    return new BitmapDrawable(bitmap);
  }

  public static Bitmap drawTextInside9PatchB(String text, int textColor,
                                             float textSize, Drawable drawable) {
    Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    paint.setTextSize(textSize);
    paint.setColor(textColor);
    paint.setTextAlign(Align.CENTER);
    int textHeight = (int) (paint.descent() - paint.ascent() + 0.5f);
    int textWidth = (int) (paint.measureText(text, 0, text.length()) + 0.5f);
    logger.d("textWidth:" + textWidth);
    Rect padding = new Rect();
    drawable.getPadding(padding);
    logger.d("paddings:" + padding.toShortString());
    Rect bounds = new Rect(0, 0, padding.left + padding.right + textWidth,
      padding.top + padding.bottom + textHeight);
    Bitmap bitmap = Bitmap.createBitmap(bounds.width(), bounds.height(),
      Config.ARGB_8888);
    Canvas canvas = new Canvas(bitmap);
    drawable.setBounds(bounds);
    drawable.draw(canvas);
    canvas.drawText(text, padding.left + textWidth / 2f, padding.top
      + textHeight - paint.descent(), paint);
    drawable.getPadding(padding);
    logger.d("paddings:" + padding.toShortString());
    return bitmap;
  }

  public static Bitmap toRoundBitmap(Bitmap bitmap) {
    int width = bitmap.getWidth();
    int height = bitmap.getHeight();
    float roundPx;
    float left, top, right, bottom, dst_left, dst_top, dst_right, dst_bottom;
    if (width <= height) {
      roundPx = width / 2;
      top = 0;
      bottom = width;
      left = 0;
      right = width;
      height = width;
      dst_left = 0;
      dst_top = 0;
      dst_right = width;
      dst_bottom = width;
    } else {
      roundPx = height / 2;
      float clip = (width - height) / 2;
      left = clip;
      right = width - clip;
      top = 0;
      bottom = height;
      width = height;
      dst_left = 0;
      dst_top = 0;
      dst_right = height;
      dst_bottom = height;
    }

    Bitmap output = Bitmap.createBitmap(width, height, Config.ARGB_8888);
    Canvas canvas = new Canvas(output);

    final int color = 0xff424242;
    final Paint paint = new Paint();
    final Rect src = new Rect((int) left, (int) top, (int) right, (int) bottom);
    final Rect dst = new Rect((int) dst_left, (int) dst_top, (int) dst_right,
      (int) dst_bottom);
    final RectF rectF = new RectF(dst);

    paint.setAntiAlias(true);

    canvas.drawARGB(0, 0, 0, 0);
    paint.setColor(color);
    canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

    paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
    canvas.drawBitmap(bitmap, src, dst, paint);
    return output;
  }

  public static Bitmap createRoundConnerPhoto(int x, int y, Bitmap image,
                                              float outerRadiusRat) {
    Drawable imageDrawable = new BitmapDrawable(image);

    Bitmap output = Bitmap.createBitmap(x, y, Config.ARGB_8888);
    Canvas canvas = new Canvas(output);

    RectF outerRect = new RectF(0, 0, x, y);

    Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    paint.setColor(Color.RED);
    canvas.drawRoundRect(outerRect, outerRadiusRat, outerRadiusRat, paint);

    paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
    imageDrawable.setBounds(0, 0, x, y);
    canvas.saveLayer(outerRect, paint, Canvas.ALL_SAVE_FLAG);
    imageDrawable.draw(canvas);
    canvas.restore();

    return output;
  }

  public static String getEditTextString(TextView et) {
    return et.getText() == null ? null : et.getText().toString().trim();
  }

  public static void showSoftInput(View view) {
    Context context = view.getContext();
    if (context == null) {
      return;
    }
    InputMethodManager manager = (InputMethodManager) context
      .getSystemService(Context.INPUT_METHOD_SERVICE);
    manager.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
  }

  public static void hideSoftInput(View view) {
    Context context = view.getContext();
    if (context == null) {
      return;
    }
    InputMethodManager manager = (InputMethodManager) context
      .getSystemService(Context.INPUT_METHOD_SERVICE);
    manager.hideSoftInputFromWindow(view.getWindowToken(), 0);
  }

  public static void setupSpinnerSelection(Spinner s, String selection) {
    if (TextUtils.isEmpty(selection)) {
      return;
    }
    for (int i = 0; i < s.getCount(); i++) {
      if (selection.equals(s.getItemAtPosition(i))) {
        s.setSelection(i);
        break;
      }
    }
  }

  private static void initWindowProp(Context context) {
    WindowManager manager = (WindowManager) context
      .getSystemService(Context.WINDOW_SERVICE);
    DisplayMetrics metric = new DisplayMetrics();
    manager.getDefaultDisplay().getMetrics(metric);
    screenWidth = metric.widthPixels;
    screenHeight = metric.heightPixels;
    density = metric.density;
  }

  public static Bitmap getAcceptableBitmapFromUri(Context context,
                                                  int maxWidth, Uri uri) throws Exception {
    InputStream is = null;
    ContentResolver cr = context.getContentResolver();
    BitmapFactory.Options opts = null;
    try {
      is = cr.openInputStream(uri);
      opts = new BitmapFactory.Options();
      opts.inJustDecodeBounds = true;
      BitmapFactory.decodeStream(is, null, opts);
      opts.inSampleSize = computeSampleSize(opts, maxWidth);
    } finally {
      if (is != null) {
        is.close();
      }
    }
    try {
      is = cr.openInputStream(uri);
      int actualWidth = opts.outWidth / opts.inSampleSize;
      int actualHeight = opts.outHeight / opts.inSampleSize;
      logger.d("outWidth:" + opts.outWidth + "actualWidth:" + actualWidth
        + "actualHeight:" + actualHeight);
      long bitmapSize = actualHeight * actualWidth * 4;
      opts.inJustDecodeBounds = false;
      opts.inPreferredConfig = bitmapSizeUnacceptable(bitmapSize) ? Config.RGB_565
        : Config.ARGB_8888;
      return BitmapFactory.decodeStream(is, null, opts);
    } finally {
      if (is != null) {
        is.close();
      }
    }
  }

  /**
   * @param maxWidth
   * @param pathName
   * @return
   */
  public static Bitmap getAcceptableBitmapFromFile(int maxWidth, String pathName) {
    BitmapFactory.Options opts = new BitmapFactory.Options();
    opts.inJustDecodeBounds = true;
    BitmapFactory.decodeFile(pathName, opts);
    System.gc();// gc before check memory
    opts.inSampleSize = computeSampleSize(opts, maxWidth);
    int actualWidth = opts.outWidth / opts.inSampleSize;
    int actualHeight = opts.outHeight / opts.inSampleSize;
    long bitmapSize = actualHeight * actualWidth * 4;
    opts.inJustDecodeBounds = false;
    opts.inPreferredConfig = bitmapSizeUnacceptable(bitmapSize) ? Config.RGB_565
      : Config.ARGB_8888;
    return BitmapFactory.decodeFile(pathName, opts);
  }

  private static boolean bitmapSizeUnacceptable(long bitmapSize) {
    Runtime runtime = Runtime.getRuntime();
    long acceptableMaxMemory = (runtime.maxMemory() - runtime.totalMemory()) / 2;
    return bitmapSize > acceptableMaxMemory;
  }

  public static String getRealPathFromURI(Uri uri, ContentResolver resolver) {
    String str = null;
    Cursor cursor = null;
    try {
      String[] proj = {MediaStore.Images.Media.DATA};
      cursor = resolver.query(uri, proj, null, null, null);
      int column_index = cursor
        .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
      cursor.moveToFirst();
      str = cursor.getString(column_index);
    } catch (Exception e) {
    } finally {
      if (cursor != null) {
        cursor.close();
      }
    }
    return str;
  }

  public static int computeAcceptableBitmapSampleSize(int maxWidth,
                                                      String pathName) {
    BitmapFactory.Options opts = new BitmapFactory.Options();
    opts.inJustDecodeBounds = true;
    BitmapFactory.decodeFile(pathName, opts);
    return computeSampleSize(opts.outWidth, opts.outHeight, maxWidth);
  }

  private static int computeSampleSize(BitmapFactory.Options options,
                                       int maxWidth) {

    int width_tmp = options.outWidth, height_tmp = options.outHeight;
    int scale = 1;
    while (true) {
      if (width_tmp / 2 < maxWidth || height_tmp / 2 < maxWidth)
        break;
      width_tmp /= 2;
      height_tmp /= 2;
      scale *= 2;
    }

    return scale;
  }

  private static int computeSampleSize(int width_tmp, int height_tmp,
                                       int maxWidth) {
    int scale = 1;
    while (true) {
      if (width_tmp / 2 < maxWidth || height_tmp / 2 < maxWidth)
        break;
      width_tmp /= 2;
      height_tmp /= 2;
      scale *= 2;
    }

    return scale;
  }
}
