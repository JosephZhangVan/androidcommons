package me.josephzhang.common.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.Base64;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class ResourceUtil {
  private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(
          "yyyy-MM-dd", Locale.CHINA);
  private static final SimpleDateFormat SIMPLE_TIME_FORMAT = new SimpleDateFormat(
          "M-d HH:mm", Locale.getDefault());
  private static final SimpleDateFormat SHORT_DATE_FORMAT = new SimpleDateFormat(
          "yyyy/MM/dd", Locale.CHINA);
  private static final SimpleDateFormat TODAY_DATE_FORMAT = new SimpleDateFormat(
          "今天 HH:mm", Locale.CHINA);
  private static final SimpleDateFormat YESTERDAY_DATE_FORMAT = new SimpleDateFormat(
          "昨天 HH:mm", Locale.CHINA);
  private static final SimpleDateFormat NORMAL_DATE_FORMAT = new SimpleDateFormat(
          "MM/dd", Locale.CHINA);

  private static final SimpleDateFormat DATE_FORMAT_SLASH = new SimpleDateFormat(
          "yyyy/MM/dd HH:mm", Locale.CHINA);

  private static final String TIME_UNKNOWN = "未知";

  public static String toPrettyPrice(String price) {
    if (price == null) {
      return price;
    }
    price = price.replace(".00", "");
    price += "元";
    return price;
  }

  public static int toNativeTime(long time) {
    Calendar c = Calendar.getInstance();
    c.setTimeInMillis(time);
    return toNativeTime(c);
  }

  public static int toNativeTime(Calendar c) {
    return c.get(Calendar.YEAR) * 10000
            + (c.get(Calendar.MONTH) - Calendar.JANUARY + 1) * 100
            + c.get(Calendar.DAY_OF_MONTH);
  }

  public static String getLocationStr(String city, String area) {
    StringBuilder sb = new StringBuilder();
    city = removeLastIfEndsWith(city, "市");
    area = removeLastIfEndsWith(area, "区");
    if (!TextUtils.isEmpty(city)) {
      sb.append(city);
    }
    if (!TextUtils.isEmpty(area)) {
      sb.append(area);
    }
    if (sb.length() <= 0) {
      sb.append("地区未知");
    }
    return sb.toString();
  }

  public static String removeLastIfEndsWith(String str, String ends) {
    if (TextUtils.isEmpty(str) || TextUtils.isEmpty(ends)) {
      return str;
    } else if (str.endsWith(ends)) {
      if (str.length() == ends.length()) {
        return null;
      } else {
        str = str.substring(0, str.length() - ends.length());
        return str;
      }
    } else {
      return str;
    }
  }

  public static String encodeBase64(Bitmap bitmap) {
    ByteArrayOutputStream os = new ByteArrayOutputStream(1024);
    BufferedOutputStream bos = new BufferedOutputStream(os);
    bitmap.compress(Bitmap.CompressFormat.JPEG, 30, bos);
    try {
      bos.flush();
      byte[] output = os.toByteArray();
      return Base64.encodeToString(output, Base64.DEFAULT);
    } catch (IOException e) {
      // should not happen
    }
    return null;
  }

  public static int dipToPx(Context context, float dp) {
    return (int) (dp * ViewUtil.getScreenDensity(context) + 0.5f);
  }

  public static int dipResourceToPx(Context context, int resourceId) {
    return (int) context.getResources().getDimension(resourceId);
  }

  public static boolean isInSameDay(Calendar c1, Calendar c2) {
    return c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR)
            && c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH)
            && c1.get(Calendar.DAY_OF_MONTH) == c2.get(Calendar.DAY_OF_MONTH);
  }

  public static String getSlashDate(long date) {
    if (date <= 0) {
      return TIME_UNKNOWN;
    }
    return DATE_FORMAT_SLASH.format(new Date(date));
  }

  public static long parseDate(String date) {
    try {
      return DATE_FORMAT.parse(date).getTime();
    } catch (ParseException e) {
      return 0l;
    }
  }

  public static String getRelativeDateString(long date) {
    if (date <= 0) {
      return TIME_UNKNOWN;
    }
    Calendar c1 = Calendar.getInstance();
    Calendar c2 = Calendar.getInstance();
    c2.setTimeInMillis(date);
    if (isInSameDay(c1, c2)) {
      return TODAY_DATE_FORMAT.format(new Date(date));
    }
    c1.add(Calendar.DATE, -1);
    if (isInSameDay(c1, c2)) {
      return YESTERDAY_DATE_FORMAT.format(new Date(date));
    }
    return NORMAL_DATE_FORMAT.format(new Date(date));
  }

  public static String getDateString(long date) {
    if (date <= 0) {
      return TIME_UNKNOWN;
    }
    return DATE_FORMAT.format(new Date(date));
  }

  public static long parseSimpleTime(String time) {
    try {
      return SIMPLE_TIME_FORMAT.parse(time).getTime();
    } catch (ParseException e) {
      return 0l;
    }
  }

  public static String getSimpleTime(long date) {
    if (date <= 0) {
      return TIME_UNKNOWN;
    }
    return SIMPLE_TIME_FORMAT.format(new Date(date));
  }

  public static String getShortTime(long date) {
    if (date <= 0) {
      return TIME_UNKNOWN;
    }
    return SHORT_DATE_FORMAT.format(new Date(date));
  }

}
