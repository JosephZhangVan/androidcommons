package me.josephzhang.common;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.widget.BaseAdapter;

import me.josephzhang.common.util.Logger;

public abstract class BaseDialogFragment extends DialogFragment {

  public interface DialogCallBack {
    boolean onDialogBtnYesClicked(int dialogId, Object... obj);

    boolean onDialogBtnNoClicked(int dialogId);

    boolean onDialogClicked(int dialogId, int what, Object... obj);

    void onDialogDismissed(int dialogId);
  }

  protected Logger logger = Logger.with(getClass());
  protected DialogCallBack callBack;

  /**
   * @return dialog id in DialogIds
   */
  public abstract int getDialogId();

  @Override
  public void onAttach(Activity activity) {
    super.onAttach(activity);
    if (activity instanceof DialogCallBack) {
      callBack = (DialogCallBack) activity;
    }
  }

  @Override
  public void onDismiss(DialogInterface dialog) {
    super.onDismiss(dialog);
    if (callBack != null) {
      callBack.onDialogDismissed(getDialogId());
    }
  }

  @Override
  public void onDetach() {
    super.onDetach();
    callBack = null;
  }

  protected FragmentActivity getValidActivity() {
    FragmentActivity a = getActivity();
    if (a != null && !a.isFinishing() && isAdded()) {
      return a;
    } else {
      return null;
    }
  }

  protected OnClickListener getOnClickListenerProxy() {
    return new OnClickListener() {

      @Override
      public void onClick(DialogInterface dialog, int which) {
        if (callBack != null) {
          if (callBack.onDialogClicked(getDialogId(), which)) {
            dismissAllowingStateLoss();
          }
        }
      }
    };
  }

  protected OnClickListener getOnClickListenerProxy(final BaseAdapter adapter) {
    return new OnClickListener() {

      @Override
      public void onClick(DialogInterface dialog, int which) {
        if (callBack != null) {
          if (which >= 0 && which < adapter.getCount())
            if (callBack.onDialogClicked(getDialogId(), which,
              adapter.getItem(which))) {
              dismissAllowingStateLoss();
            }
        }
      }
    };
  }

}
