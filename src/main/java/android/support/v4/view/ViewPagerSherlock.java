package android.support.v4.view;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewParent;
import android.widget.Scroller;
import me.josephzhang.common.util.ActivityUtil;
import me.josephzhang.common.util.Logger;

import java.lang.reflect.Field;

/**
 * A secret agent inside android's package to enable more essential features of ViewPager.
 * <p>
 * Created by joseph on 5/20/14.
 */
public class ViewPagerSherlock extends ViewPager {
  private Handler handler = new Handler();
  private Logger logger = Logger.with(getClass());
  private int velocity = 1;
  private boolean velocitySet = false;
  private boolean lockPager = false;
  private boolean disallowIntercept = false;
  private boolean autoScroll = false;
  private long scrollPeriod = 5000;
  private Thread autoScrollThread;
  private boolean isAttachedToWindow = false;

  public ViewPagerSherlock(Context context) {
    this(context, null);
  }

  public ViewPagerSherlock(Context context, AttributeSet attrs) {
    super(context, attrs);
    initInternal(context);
  }

  @Override
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();
    isAttachedToWindow = true;
    if (autoScroll) {
      startAutoScroll();
    }
  }

  @Override
  protected void onDetachedFromWindow() {
    super.onDetachedFromWindow();
    isAttachedToWindow = false;
    stopAutoScroll();
  }

  public void onActivityPause() {
    stopAutoScroll();
  }

  public void onActivityResume() {
    if (autoScroll) {
      startAutoScroll();
    }
  }

  public void setAutoScrollEnabled(boolean enabled) {
    if (autoScroll = enabled) {
      startAutoScroll();
    } else {
      stopAutoScroll();
    }
  }

  public boolean setScroller(Scroller scroller) {
    try {
      Field field = ViewPager.class.getDeclaredField("mScroller");
      field.setAccessible(true);
      field.set(this, scroller);
      return true;
    } catch (Exception e) {
      logger.e(e);
      return false;
    }
  }

  public void setAnimSlideVelocity(int velocity) {
    this.velocity = velocity;
    velocitySet = true;
  }

  public void lockPager(boolean lock) {
    this.lockPager = lock;
  }

  public void setDisallowIntercept(boolean disallowIntercept) {
    this.disallowIntercept = disallowIntercept;
  }

  @Override
  void setCurrentItemInternal(int item, boolean smoothScroll, boolean always) {
    if (velocitySet) {
      setCurrentItemInternal(item, smoothScroll, always, velocity);
    } else {
      super.setCurrentItemInternal(item, smoothScroll, always);
    }
  }

  @Override
  public boolean onInterceptTouchEvent(MotionEvent ev) {
    if (lockPager) {
      return false;
    } else {
      boolean res = false;
      try {
        res = super.onInterceptTouchEvent(ev);
      } catch (IllegalArgumentException e) {
        logger.w(e);
      } catch (ArrayIndexOutOfBoundsException e) {
        logger.w(e);
      }
      if (disallowIntercept) {
        ViewParent parent = getParent();
        if (parent != null) {
          parent.requestDisallowInterceptTouchEvent(true);
        }
      }
      return res;
    }
  }

  @Override
  public boolean onTouchEvent(MotionEvent ev) {
    if (lockPager) {
      return false;
    } else {
      if (autoScroll) {
        switch (ev.getAction()) {
          case MotionEvent.ACTION_DOWN:
          case MotionEvent.ACTION_MOVE:
            stopAutoScroll();
            break;
          case MotionEvent.ACTION_CANCEL:
          case MotionEvent.ACTION_UP:
            startAutoScroll();
            break;
        }
      }
      boolean res = false;
      try {
        res = super.onTouchEvent(ev);
      } catch (IllegalArgumentException e) {
        logger.w(e);
      } catch (ArrayIndexOutOfBoundsException e) {
        logger.w(e);
      }
      if (disallowIntercept) {
        ViewParent parent = getParent();
        if (parent != null) {
          parent.requestDisallowInterceptTouchEvent(true);
        }
      }
      return res;
    }
  }

  private void initInternal(Context context) {

  }

  private void startAutoScroll() {
    if (!ActivityUtil.checkThread(autoScrollThread) && isAttachedToWindow) {
      autoScrollThread = new Thread() {
        @Override
        public void run() {
          try {
            while (!Thread.interrupted()) {
              sleep(scrollPeriod);
              PagerAdapter adapter = getAdapter();
              if (adapter != null) {
                final int current = getCurrentItem();
                int count = adapter.getCount();
                if (current < count) {
                  handler.post(new Runnable() {
                    @Override
                    public void run() {
                      setCurrentItemInternal(current + 1, true, false, 300);
                      // setCurrentItem(current + 1, true);
                    }
                  });
                }
              }
            }
          } catch (InterruptedException e) {
          }
        }
      };
      autoScrollThread.start();
    }
  }

  private void stopAutoScroll() {
    handler.removeCallbacksAndMessages(null);
    if (ActivityUtil.checkThread(autoScrollThread)) {
      autoScrollThread.interrupt();
      autoScrollThread = null;
    }
  }

}
